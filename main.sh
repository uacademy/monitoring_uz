#!/bin/bash

LOG_DIR=/var/log/monitoring_uz
BOT_TOKEN=6764176034:AAG3WpcxDgFhdFQiEnI2uFWJ7JOcuuwWkRk
CHAT_ID=-1001531463654
CHECKING_INTERVAL=5
CPU_PERCENT=20
CPU_SEQUENCE_LIMIT=3
STATE_PATH=/etc/monitoring_uz/state

init_check(){
	if [[ ! -d "$STATE_PATH" ]]
	then
		mkdir -p "$STATE_PATH"
	fi
	if [[ ! -d "$LOG_DIR" ]]
	then
		mkdir -p "$LOG_DIR"
	fi
	rm -f $STATE_PATH/*
}

check_cpu() {
  current_cpu_load=$(uptime | awk '{ print $11 }' | tr -d ',')
  cpu_count=$(nproc)
  current_cpu_load_percent=$(echo "$current_cpu_load*$cpu_count*100" | bc)
  current_cpu_load_percent=${current_cpu_load_percent%.*}
  check_date=$(date)
  echo "$check_date | CPU checked: ${current_cpu_load_percent}%" >> $LOG_DIR/cpu.log
  if [[ $current_cpu_load_percent -ge $CPU_PERCENT && ! -f $STATE_PATH/cpu_in_alarm ]]
  then
	count_cpu_over_metrics=$(cat $STATE_PATH/count_cpu_over_metrics 2>/dev/null)
	if [[ -z $count_cpu_over_metrics ]]
	then
		echo "2" > $STATE_PATH/count_cpu_over_metrics
	elif [[ $count_cpu_over_metrics -lt $CPU_SEQUENCE_LIMIT ]]
	then
		echo $((count_cpu_over_metrics + 1)) > $STATE_PATH/count_cpu_over_metrics
	else
		message=$(echo -e "Alarm \nCPU load percent: $current_cpu_load_percent")
		send_alarm "$message"
		> $STATE_PATH/count_cpu_over_metrics
		touch $STATE_PATH/cpu_in_alarm
	fi
  elif [[ $current_cpu_load_percent -lt $CPU_PERCENT && -f $STATE_PATH/cpu_in_alarm ]]
  then
	  message=$(echo -e "Resolved \nCPU load percent: $current_cpu_load_percent")
	  send_alarm "$message"
	  rm -f $STATE_PATH/cpu_in_alarm
	  > $STATE_PATH/count_cpu_over_metrics
  else
	  > $STATE_PATH/count_cpu_over_metrics
  fi
}

send_alarm() {
	echo "$1"
	message="$1"
	curl -s --data "text=$message" --data "chat_id=$CHAT_ID" 'https://api.telegram.org/bot'$BOT_TOKEN'/sendMessage' > /dev/null
}

init_check

while [[ -f /var/mon ]]
do
	check_cpu
	sleep $CHECKING_INTERVAL
done
